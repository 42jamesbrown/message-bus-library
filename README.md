# Message bus library

A library to separate layers of applications and send data and execute business logic through messages.